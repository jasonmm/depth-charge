(ns com.shadybrooksoftware.depth-charge.new-game-form.subs
  (:require
    [re-frame.core :refer [reg-sub]]))

(reg-sub
  ::form-field-value
  (fn [db [_ field-id]]
    (if-let [v (get-in db [:new-game-form field-id])]
      v
      (js/console.error (str "Invalid field ID: " field-id)))))

(reg-sub
  ::ship
  :<- [::form-field-value :ship]
  (fn [ship] ship))

(reg-sub
  ::dimension
  :<- [::form-field-value :dimension]
  (fn [dimension] dimension))
