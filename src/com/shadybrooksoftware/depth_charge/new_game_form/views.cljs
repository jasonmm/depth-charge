(ns com.shadybrooksoftware.depth-charge.new-game-form.views
  (:require
    [re-frame.core :as re]
    [com.shadybrooksoftware.depth-charge.new-game-form.subs :as subs]
    [com.shadybrooksoftware.depth-charge.new-game-form.events :as events]
    [com.shadybrooksoftware.depth-charge.utils :refer [<sub] :as u]
    [com.shadybrooksoftware.depth-charge.ui.labels :as ui.l]
    [com.shadybrooksoftware.depth-charge.ui.buttons :as ui.btn]))

(defn- select
  [props options]
  [:select.p-3.rounded
   props
   (for [option options]
     [:option
      (assoc option :key (:value option))
      (:label option)])])

(defn- form-field
  [label field]
  [:div.flex.flex-row.items-center.justify-between.px-6.my-2
   label
   field])

(defn- destroyer-text
  []
  [:<>
   [:h3.text-md.md:text-xl.text-slate-400.italic.font-serif "Hunt The Hunter!"]
   [:div.text-xs.mx-6.font-mono
    [:p.my-2
     (str "You are captain of the destroyer USS Computer. An enemy submarine"
          " has been causing trouble and your mission is to destroy it.")]
    [:p.my-2
     (str "You may select the size of the cube of water you wish to"
          " search in. The computer then determines how many depth charges"
          " you get to destroy the submarine.")]
    [:p.my-2
     (str "Each depth charge is exploded by specifying a trio of numbers."
          " The first two numbers are the surface coordinates (X,Y), the third is"
          " the depth. After each depth charge, your sonar observer will"
          " tell you where the explosion was relative to the submarine.")]]])

(defn new-game-form
  []
  (let [ship      (<sub [::subs/ship])
        dimension (<sub [::subs/dimension])]
    [:<>
     (case ship
       :destroyer [destroyer-text])
     [:div.w-full
      [form-field
       [ui.l/label "Choose Your Ship"]
       [select
        {:value     ship
         :on-change #(re/dispatch [::events/set-ship (u/target-value %)])}
        [{:value :destroyer
          :label "Destroyer"}
         {:value    :submarine
          :label    "Submarine"
          :disabled true}]]]
      [form-field
       [ui.l/label "Cube Dimension"]
       [select
        {:value     dimension
         :on-change #(re/dispatch [::events/set-dimension (u/target-value %)])}
        (map (fn [n] {:value n :label n})
             [5 10 20 30 40 50])]]
      [ui.btn/primary
       {:class    "mt-3"
        :on-click #(re/dispatch [::events/new-game])}
       "Start Game"]]]))
