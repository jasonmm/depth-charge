(ns com.shadybrooksoftware.depth-charge.new-game-form.events
  (:require
    [re-frame.core :refer [reg-event-db reg-event-fx]]
    [com.shadybrooksoftware.depth-charge.events :as events]))

(reg-event-db
  ::set-ship
  (fn [db [_ ship]]
    (assoc-in db [:new-game-form :dimension] ship)))

(reg-event-db
  ::set-dimension
  (fn [db [_ dimension]]
    (assoc-in db [:new-game-form :dimension] (js/parseInt dimension))))

(reg-event-fx
  ::new-game
  (fn [{:keys [db]} _]
    {:fx [[:dispatch [::events/new-game (:new-game-form db)]]]}))
