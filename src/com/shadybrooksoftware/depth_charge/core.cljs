(ns com.shadybrooksoftware.depth-charge.core
  (:require
    ["react-dom/client" :refer [createRoot]]
    [reagent.core :as r]
    [re-frame.core :as re-frame]
    [com.shadybrooksoftware.depth-charge.routes :as routes]
    [com.shadybrooksoftware.depth-charge.events :as events]
    [com.shadybrooksoftware.depth-charge.views :as views]))

(def debug?
  ^boolean goog.DEBUG)

; See https://stackoverflow.com/a/72477660
(defonce root (createRoot (.getElementById js/document "app")))

(defn dev-setup
      []
      (when debug?
                (enable-console-print!)
                (println "dev mode")))

(defn mount-root
      []
      (re-frame/clear-subscription-cache!)
      (.render root (r/as-element [views/main-panel])))

(defn ^:export init
      []
      (routes/start!)
      (re-frame/dispatch-sync [::events/initialize-app])
      (dev-setup)
      (mount-root))

(defn ^:dev/after-load re-render
      []
      ;; The `:dev/after-load` metadata causes this function to be called
      ;; after shadow-cljs hot-reloads code.
      ;; This function is called implicitly by its annotation.
      (mount-root))
