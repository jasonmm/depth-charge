(ns com.shadybrooksoftware.depth-charge.routes
  (:require
    [re-frame.core :as re]
    [goog.events :as gevents]
    [goog.history.EventType :as EventType]
    [bidi.bidi :as bidi]
    [com.shadybrooksoftware.depth-charge.events :as events])
  (:import goog.History))

(def routes
  ["/" {"" :home}])

(defn on-navigate
  [^js/goog.events.EventType e]
  (re/dispatch [::events/route-dispatch (or (bidi/match-route routes (.-token e))
                                            {:handler :home})]))

(defn start!
  []
  (doto (History.)
    (gevents/listen EventType/NAVIGATE on-navigate)
    (.setEnabled true)))

;; -- url-for -----------------------------------------------------------------
;; To dispatch routes in our UI (view) we will use url-for and then pass a
;; keyword to which route we want to direct the user.
;; usage: (url-for :home)
(def url-for #(str "#" (bidi/path-for routes %)))
