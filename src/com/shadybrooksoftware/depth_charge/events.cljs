(ns com.shadybrooksoftware.depth-charge.events
  (:require
    [re-frame.core :refer [reg-event-fx reg-event-db reg-fx]]
    [com.shadybrooksoftware.depth-charge.game :as g]
    [com.shadybrooksoftware.depth-charge.utils :as utils]))

(reg-fx
  :set-route
  (fn [{:keys [url]}]
    (utils/set-hash! url)))

(reg-event-fx
  ::initialize-app
  (fn [_ _]
    {:db {:new-game-form {:ship      :destroyer
                          :dimension 20}}}))

(reg-event-fx
  ::route-dispatch
  (fn [_ [_ route-info]]
    (let [route-id (:handler route-info)]
      (case route-id
        :home {:dispatch [::set-active-panel :home-panel]}
        :game-board {:dispatch [::set-active-panel :game-board]}
        {:dispatch [::set-active-panel :invalid-route]}))))

(reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))

(reg-event-db
  ::new-game
  (fn [db [_ {:keys [ship dimension]}]]
    (assoc db :game (g/new-game ship dimension))))

(reg-event-db
  ::set-x
  (fn [db [_ x]]
    (assoc-in db [:depth-charge :x] (parse-long x))))

(reg-event-db
  ::set-y
  (fn [db [_ y]]
    (assoc-in db [:depth-charge :y] (parse-long y))))

(reg-event-db
  ::set-z
  (fn [db [_ z]]
    (assoc-in db [:depth-charge :z] (parse-long z))))

(reg-event-db
  ::launch-depth-charge
  (fn [db]
    (-> db
        (update :game
                g/launch-depth-charge
                (get-in db [:depth-charge :x])
                (get-in db [:depth-charge :y])
                (get-in db [:depth-charge :z]))
        (assoc-in [:depth-charge :x] "")
        (assoc-in [:depth-charge :y] "")
        (assoc-in [:depth-charge :z] ""))))

(reg-event-db
  ::close-game
  (fn [db]
    (dissoc db :game)))
