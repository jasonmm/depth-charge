(ns com.shadybrooksoftware.depth-charge.history
  (:require
    [com.shadybrooksoftware.depth-charge.subs :as subs]
    [com.shadybrooksoftware.depth-charge.utils :refer [<sub]]))

(defn coordinates
  [[x y z]]
  [:span.font-bold x ", " y ", " z])

(defn- depth-charge-launched
  [event]
  [:<>
   [:div "Depth Charge #" (:depth-charge-number event) " Launched at "
    [coordinates (:coordinates event)]]
   [:div "Sonar reports "
    [:span.font-bold (:sonar-report event)]]])

(defn- generic
  [event]
  [:<>
   [:p (:event event)]])

(defn history
  []
  (let [events (reverse (<sub [::subs/display-events]))]
    [:div
     [:h3.text-sm.font-bold.font-mono "History"]
     (for [event events]
       [:div.bg-stone-300.rounded.w-full.my-2.p-2 {:key (random-uuid)}
        (case (:event event)
          :depth-charge [depth-charge-launched event]
          [generic event])])]))
