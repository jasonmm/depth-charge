(ns com.shadybrooksoftware.depth-charge.subs
  (:require
    [re-frame.core :refer [reg-sub]]
    [com.shadybrooksoftware.depth-charge.game :as g]))

(reg-sub ::active-panel (fn [db _] (:active-panel db)))
(reg-sub ::game (fn [db] (:game db)))
(reg-sub ::depth-charge-x (fn [db] (get-in db [:depth-charge :x])))
(reg-sub ::depth-charge-y (fn [db] (get-in db [:depth-charge :y])))
(reg-sub ::depth-charge-z (fn [db] (get-in db [:depth-charge :z])))

(reg-sub
  ::game-events
  :<- [::game]
  (fn [game]
    (:events game)))

(reg-sub
  ::display-events
  :<- [::game-events]
  (fn [events]
    (filter #(contains? g/display-events (:event %)) events)))

(reg-sub
  ::board-dimension
  :<- [::game]
  (fn [game]
    (get-in game [:board :dimension])))

(reg-sub
  ::destroyer
  :<- [::game]
  (fn [game]
    (:destroyer game)))

(reg-sub
  ::destroyer-depth-charges
  :<- [::game]
  (fn [game]
    (get-in game [:destroyer :depth-charges])))

(reg-sub
  ::sub-sunk?
  :<- [::game]
  (fn [game]
    (get-in game [:submarine :sunk?])))

(reg-sub
  ::depth-charge-launchable?
  :<- [::depth-charge-x]
  :<- [::depth-charge-y]
  :<- [::depth-charge-z]
  :<- [::game]
  (fn [[x y z game]]
    (let [dimension (get-in game [:board :dimension])
          sub-sunk? (get-in game [:submarine :sunk?])]
      (and x
           y
           z
           (< -1 x dimension)
           (< -1 y dimension)
           (< -1 z dimension)
           (not sub-sunk?)))))

(reg-sub
  ::game-over?
  :<- [::destroyer-depth-charges]
  :<- [::sub-sunk?]
  (fn [[remaining-depth-charges submarine-sunk?]]
    (or (zero? remaining-depth-charges)
        submarine-sunk?)))

(reg-sub
  ::submarine-coordinates
  :<- [::game]
  (fn [game]
    (get-in game [:submarine :coordinates])))

(reg-sub
  ::game-over-reason
  :<- [::destroyer-depth-charges]
  :<- [::sub-sunk?]
  (fn [[remaining-depth-charges submarine-sunk?]]
    (cond
      (zero? remaining-depth-charges)
      "You ran out of depth charges and have been torpedoed!"

      submarine-sunk?
      "BOOM!! You sunk the submarine!")))
