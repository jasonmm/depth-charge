(ns com.shadybrooksoftware.depth-charge.ui.input)

(defn input
  [props]
  [:input.rounded.p-2.bg-slate-400.text-end
   props])
