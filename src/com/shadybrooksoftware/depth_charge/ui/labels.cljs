(ns com.shadybrooksoftware.depth-charge.ui.labels)

(defn label
  [text]
  [:label.font-bold.font-serif text])
