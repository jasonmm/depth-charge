(ns com.shadybrooksoftware.depth-charge.ui.buttons)

(defn primary
  [props label]
  [:div.flex.justify-center.p-3
   [:button.py-3.px-6.bg-sky-300.text-black.rounded-xl.hover:bg-sky-900.hover:text-white.duration-300.disabled:bg-stone-200.disabled:text-stone-400.disabled:italic
    props
    label]])
