(ns com.shadybrooksoftware.depth-charge.game)

(def display-events
  "The set of event identifiers that should be displayed in the history section
  of the UI."
  #{:depth-charge :submarine-sunk :destroyer-sunk})

(defn dimension->depth-charges
  "Return the number of depth charges for the given cube dimension."
  [dimension]
  (-> (quot (js/Math.log dimension)
            (js/Math.log 2))
      inc))

(defn new-game
  "Return a map representing a new game."
  [ship dimension]
  (let [num-depth-charges (dimension->depth-charges dimension)
        sub-position      [(rand-int dimension)
                           (rand-int dimension)
                           (rand-int dimension)]]
    {:id        (random-uuid)
     :player    {:ship ship}
     :destroyer {:initial-depth-charges num-depth-charges
                 :depth-charges         num-depth-charges}
     :submarine {:can-move?      false
                 :can-attack?    false
                 :sunk?          false
                 :coordinates    sub-position
                 :start-position sub-position}
     :board     {:dimension dimension}
     :events    [{:event :game-created}]}))

(defn submarine-sunk?
  [game x y z]
  (= (get-in game [:submarine :coordinates])
     [x y z]))

(defn sunk-report
  [tries]
  (str "BOOM! You sunk the submarine "
       (if (= 1 tries) "on your first try" (str "in " tries " tries"))))

(defn sonar-report
  [game x y z]
  (let [[sx sy sz] (get-in game [:submarine :coordinates])]
    (if (submarine-sunk? game x y z)
      (sunk-report (- (get-in game [:destroyer :initial-depth-charges])
                      (get-in game [:destroyer :depth-charges])))
      (cond-> ""
        (> y sy) (str "NORTH")
        (< y sy) (str "SOUTH")
        (> x sx) (str "EAST")
        (< x sx) (str "WEST")
        (or (not= y sy)
            (not= x sx)) (str " and ")
        (> z sz) (str " TOO LOW")
        (< z sz) (str " TOO HIGH")
        (= z sz) (str " DEPTH OK")))))

(defn launch-depth-charge
  "Return the new game state after a depth charge was launched at the passed
  in coordinates."
  [game x y z]
  (let [sunk?               (submarine-sunk? game x y z)
        game                (-> game
                                (assoc-in [:submarine :sunk?] sunk?)
                                (update-in [:destroyer :depth-charges] dec))
        depth-charge-number (- (get-in game [:destroyer :initial-depth-charges])
                               (get-in game [:destroyer :depth-charges]))]
    (update game :events conj {:event               :depth-charge
                               :depth-charge-number depth-charge-number
                               :coordinates         [x y z]
                               :sonar-report        (sonar-report game x y z)
                               :sunk?               sunk?})))
