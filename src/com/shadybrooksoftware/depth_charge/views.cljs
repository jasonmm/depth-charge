(ns com.shadybrooksoftware.depth-charge.views
  (:require
    [re-frame.core :as re]
    [com.shadybrooksoftware.depth-charge.subs :as subs]
    [com.shadybrooksoftware.depth-charge.events :as events]
    [com.shadybrooksoftware.depth-charge.new-game-form.views :as ngf.views]
    [com.shadybrooksoftware.depth-charge.history :as h]
    [com.shadybrooksoftware.depth-charge.ui.buttons :as ui.btn]
    [com.shadybrooksoftware.depth-charge.ui.labels :as ui.l]
    [com.shadybrooksoftware.depth-charge.ui.input :as ui.input]
    [com.shadybrooksoftware.depth-charge.utils :refer [<sub] :as u]))

(defn- container
  [body]
  [:div.flex.flex-col.justify-center.items-center.max-w-md.m-6.pb-10.mx-auto body])

(defn coordinates-form []
  (let [launchable? (<sub [::subs/depth-charge-launchable?])
        x           (<sub [::subs/depth-charge-x])
        y           (<sub [::subs/depth-charge-y])
        z           (<sub [::subs/depth-charge-z])
        sub-sunk?   (<sub [::subs/sub-sunk?])]
    [:<>
     [:div.flex.flex-row.justify-between.my-2
      [:div.space-x-4
       [ui.l/label "X:"]
       [ui.input/input {:size      2
                        :value     x
                        :on-change #(re/dispatch [::events/set-x (u/target-value %)])}]]
      [:div.space-x-4
       [ui.l/label "Y:"]
       [ui.input/input {:size      2
                        :value     y
                        :on-change #(re/dispatch [::events/set-y (u/target-value %)])}]]
      [:div.space-x-4
       [ui.l/label "Depth:"]
       [ui.input/input {:size      2
                        :value     z
                        :on-change #(re/dispatch [::events/set-z (u/target-value %)])}]]]
     (when-not sub-sunk?
       [:div
        [ui.btn/primary
         {:disabled (not launchable?)
          :on-click #(re/dispatch [::events/launch-depth-charge])}
         (if launchable?
           "Launch Depth Charge"
           "Enter Coordinates")]])]))

(defn game-over []
  (let [reason      (<sub [::subs/game-over-reason])
        win?        (<sub [::subs/sub-sunk?])
        sub-coords  (<sub [::subs/submarine-coordinates])
        div-classes (if win?
                      "bg-emerald-500 text-black shadow-2xl"
                      "bg-stone-300")]
    [:div.p-3.my-2.rounded-xl.text-center
     {:class div-classes}
     [:h3.text-2xl.mb-2.font-bold "Game Over!"]
     [:p.font-bold reason]
     (when-not win?
       [:p "The submarine was at " (h/coordinates sub-coords)])
     [ui.btn/primary
      {:on-click #(re/dispatch [::events/close-game])}
      "Close Game"]]))

(defn game-board
  []
  (let [remaining-depth-charges (<sub [::subs/destroyer-depth-charges])
        board-dimension         (<sub [::subs/board-dimension])
        game-over?              (<sub [::subs/game-over?])]
    [:div.flex.flex-col.w-full.justify-start
     [:div.flex.flex-row.justify-between
      [:div.font-bold (str "Depth Charges: " remaining-depth-charges)]
      [:div.font-bold (str "Search Size: " board-dimension)]]
     (if-not game-over?
       [coordinates-form]
       [game-over])
     [:hr.w-full.my-3]
     [h/history]]))

(defn home-panel []
  (let [game (<sub [::subs/game])]
    [container
     [:<>
      [:h1.text-4xl.md:text-5xl.text-sky-300.uppercase.font-mono.drop-shadow-xl "Depth Charge!"]
      [:hr.w-full.my-3]
      (if game
        [game-board]
        [ngf.views/new-game-form])]]))

(defn main-panel
  []
  (case (<sub [::subs/active-panel])
    :home-panel [home-panel]
    [:div "Invalid panel"]))
